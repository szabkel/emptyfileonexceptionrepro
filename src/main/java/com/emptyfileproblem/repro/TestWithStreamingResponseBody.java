package com.emptyfileproblem.repro;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;

@RestController
@RequestMapping("api")
public class TestWithStreamingResponseBody {

    @Autowired
    ResourceLoader resourceLoader;

    @GetMapping(value = "/download2")
    public StreamingResponseBody handle(HttpServletResponse response) {
        return outputStream -> {
            var resource = resourceLoader.getResource("classpath:TestFileToRead.txt");
            response.setContentType("application/x-download");
            response.setHeader("Content-Disposition", "attachment; filename=\"SomeFile.txt\"");

            if (1 == 1 / 1) {
                throw new RuntimeException("Test");
            }

            try (var inputStream = new FileInputStream(resource.getFile())) {
                StreamUtils.copy(inputStream, outputStream);
            }
        };
    }
}
