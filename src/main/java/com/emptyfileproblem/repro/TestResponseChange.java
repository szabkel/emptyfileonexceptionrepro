package com.emptyfileproblem.repro;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.OutputStream;

@RestController
@RequestMapping("api")
public class TestResponseChange {
    @Autowired
    ResourceLoader resourceLoader;

    @GetMapping("/download-change")
    public void download(HttpServletResponse response) throws SomeException {

        try (OutputStream out = response.getOutputStream()) {
            response.setContentType("application/x-download");
            response.setHeader("Content-Disposition", "attachment; filename=\"SomeFile.txt\"");

            if (1 == 1 / 1) {
                throw new Exception("Test");
            }

            var resource = resourceLoader.getResource("classpath:TestFileToRead.txt");
            try (var inputStream = new FileInputStream(resource.getFile())) {
                StreamUtils.copy(inputStream, out);
            }
        } catch (Exception e) {
            response.setContentType("text/plain");
            response.setHeader("Content-Disposition", null);
            throw new SomeException(e);
        }
    }

    @ExceptionHandler(SomeException.class)
    public ResponseEntity<String> handleException(SomeException ex) {
        System.out.println("Failed. " + ex.getMessage());
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
